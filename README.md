# Ansible & Terraform


https://medium.com/on-the-cloud/one-click-environment-creation-with-terraform-ansible-in-under-10-6e8d9284f60


## Step 1 - let ansible execute TerraForm, and create a inventory
```
ansible-playbook -l localhost install_h2o.yml 
```

## Step 2 - use the inventory

```
ansible-playbook -i h2o_inventory.yml -l h2o install_h2o.yml
```